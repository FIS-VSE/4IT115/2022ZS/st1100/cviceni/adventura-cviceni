module cz.vse.java.xname.adventura1100 {
    requires javafx.controls;
    requires javafx.fxml;


    opens cz.vse.java.xname.adventura1100.main to javafx.fxml;
    exports cz.vse.java.xname.adventura1100.main;
}